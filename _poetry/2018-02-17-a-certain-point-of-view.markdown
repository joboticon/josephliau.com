---
layout: poetry
title:  "A Certain Point of View"
date:   2018-02-17 09:00:00 -0700
categories: poetry
author: Luke Skywalker
summary: It's like... something out of a dream or...
featureimage: /images/2018/02/pov.jpg
---
<br />Let the past die upon the fire 
<br />Leave all that I begot
<br />I was to fall upon a pyre
<br />I am the last you sought
<br />A failure of my own desire
<br />A future fleeting thought
<br />Repeating stories not entire
<br />Return to truth we wrought
<br />Now a legend must retire
<br />Or am I slow ask or go
<br />To a place become much higher
<br />And not the way you thought
<br />Let it be known as I expire
<br />I am nought I am not
<br />A liar

