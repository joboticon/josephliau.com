---
layout: poetry
title:  "I don't like sand"
date:   2020-08-31 09:00:00 -0700
categories: poetry
author: Anakin Skywalker
summary: "A prophecy that misread could have been."
featureimage: /images/2020/08/sand.jpg
---
<i>"I don't like sand. 
<br />It's coarse and rough 
<br />And irritating, 
<br />And it gets everywhere. 

<i>Not like here. 
<br />Here, 
<br />Everything is soft 
<br />And smooth." [1]

I don't like rocks.
<br />The cliffs and bluffs.
<br />Dead, unchanging,
<br />And going nowhere.

Not like here.
<br />Here,
<br />Everything can grow
<br />And move.

I like it here;
<br />Here 
<br />With you,
<br />My love.

Not like there.
<br />There
<br />Is nothing to see.
<br />Not anywhere.

--
[1] Attack of the Clones

