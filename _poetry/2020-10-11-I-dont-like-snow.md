---
layout: poetry
title:  "I don't like snow"
date:   2020-10-11 07:00:00 -0700
categories: poetry
author: Darth Vader
summary: "There's something not right. I feel cold."
featureimage: /images/2020/10/snow.jpg
---
I don't like snow.
<br />It's like frozen sand,
<br />And cracks under my steps,
<br />And echoes in my memories.

I see you there,
<br />There
<br />In the blankness;
<br />In the blinding light.

I don't like wind.
<br />It's cold and elusive.
<br />It creeps into my joints,
<br />And snips at my heart.

I lose you there,
<br />There
<br />In frosty air--
<br />The mist.

I like it here--
<br />Here
<br />In the darkness--
<br />Blind to you.

You're not there.
<br />There
<br />Is nothing left,
<br />But icy ashes.

