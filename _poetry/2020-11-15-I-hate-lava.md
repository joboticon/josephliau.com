---
layout: poetry
title:  "I hate lava"
date:   2020-11-15 07:00:00 -0700
categories: poetry
author: Darth Vader
summary: "He's more machine than man now..."
featureimage: /images/2020/11/lava.jpg
---
I hate lava.
<br />It's undying sand,
<br />And it's unburnable,
<br />Yet it burns hotter than the suns.

I am here.
<br />Here,
<br />Aflame in wrath,
<br />And regret.

I hate obsidian--
<br />Eternal lava--
<br />Final judgement--
<br />Prevailing death.

I am there.
<br />There,
<br />Immortalized in the dark
<br />Shards of pain.

Find me here:
<br />Here
<br />I the darkness
<br />Where I have power.

Are you there?
<br />There
<br />You were.
<br />I felt it.


