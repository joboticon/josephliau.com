---
layout: post
title:  "Existence is Key"
date:   2019-03-10 09:00:00 -0700
categories: community technology
author: Joseph Liau
summary: Who is the author of your reality?
featureimage: /images/2019/03/key.jpg
---

## Reality

When we type on a keyboard, we put our will and intention into writing; our goal is not merely to press keys, but rather, we desire to see our expression on the screen. If the writing is the true reality of our intentions, then what is the keyboard beyond a vehicle towards reality?

## Vanity

We exist, but as keys on a keyboard. Our existence is important. Our function is essential. But, our individual will is overrated. We have a choice to be useful or utterly useless. As one key, if we are unaware and uncooperative with the greater community, then how can we produce an accurate reality?  Unless we are connected to the output and work with the entire body in the right capacity, then our malfunction delays the reality of the author. Individually, you can only express one letter of the whole, and at best, out of yourself, you can only influence the other keys around you. Trying to create something on your own is pure vanity. Only the author knows the true reality, and can make good use of you or not.

## Purpose

Often we ask why something happens to us. Our view is narrow and self-centred. We can merely see those around us, but we don't naturally have a view of the reality of our existence and usefulness. We are constantly pressed, and it is easy to feel the pressure, and ask “why?” We must *shift* our focus from the fact that we are being pressed, and realized what is ex-pressed through us.

## Allowance

The more we fight back, the less can be expressed through us. We can resist and stick, but ultimately to fulfill our purpose, we have to give up on ourselves, and realize that we are part of a greater whole, and part of a greater purpose.

## Authorship

So, the question is, who are you allowing to author your reality? Is it your self? Or do you believe in a more important reality? If you are not pressed by a something greater--a greater purpose--then you will be limited by your narrow reality and your single existence e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e e .
