---
layout: post
title:  "Man-Machine Interface"
date:   2019-03-19 09:00:00 -0700
categories: community technology
author: Joseph Liau
summary: What makes us make technology make us human?
featureimage: /images/2019/03/gits.jpg
---

## Dystopian Destiny

We are bound to the rules of the worldly system, but we long for transcendence; we are in the world, but we are not of the world itself. Yet we have nostalgia for things of this world, and our technological innovation is fuelled by our longing for the future our past minds.

## Convergence

In the future, *you* are the computer. 

We have access to our own consciousness at all times. When we use technology we are simply transferring our consciousness into the devices and interacting with the applications that we need.

The hardware is merely a shell, much like our bodies are just a shell for our souls and spirits. The hardware is almost irrelevant except that it allows us to interact with the physical world. If we have means of transferring consciousness, then we are no longer trapped by our shells. 

Right now, we are not cyborgs, but our devices are extensions of our selves. The best representative of your cyborg-self is your mobile device. It goes with you everywhere and is a representation of who you are. It is your ghost in a shell.

You need only but to plug in yourself--your device--into other systems for greater power and productivity. However, your consciousness can't really be in more than one place at a time, and it can't necessarily do two things at once. 

## Subconscious Computing

There is also a need for a subconscious. There is a need for the storage and backup. This is something that your conscious mind does not do on its own. Similarly, your device has a backup system, and background processes. Yet, it is still confined to one location at one time.

It might be unhealthy if you have multiple consciousnesses in one shell, or to be running too many processes at one time, and that is only because we are human. Do we want our technology to make us *more* human, and thus farther removed from the technology itself, or do we want technology to make us *more than* human?

Technology can allow us to be *better* humans. A lot of technology is designed to take advantage of our human weaknesses, instead of enhancing our strengths, or filling in gaps. We can advance humanity if we stop basing technology on it. And, [maybe we can save humanity if we put more focus on it](http://josephliau.com/community/technology/2017/12/25/robota.html).

## Stand Alone Complex

A human is confined to one instance of itself. It is consolidated. So, consolidation of technology is simply confining our human self into a digital self. It is a paradox in which the evolution of technology could, in turn, be a de-evolution of humanity. How many times have you said, "if only there were two of me?" or "if only I could be in two places at the same time." From a standpoint of productivity and power, that would be great. But, logistically having two or more "yous" (body, soul and spirit) would bring many challenges.

It may true that we don't need less of "us", but we also don't need more of "us". We need different instances of "us", doing different things at different times ... but at the same time. A bunch of productive, consolidated individuals is far less valuable that a well-orchestrated group with a shared vision. This can be achieved through community--the greatest of our lost technologies. Humans don’t always stand alone, and devices don’t have to either.

## The Internet of You

We are “always connected”, but we are not always connected properly to each other, or to the [greater purpose](http://josephliau.com/community/technology/2019/03/10/existence-is-key.html). As we grow and innovate our technology, and as it grows and becomes more connected, it is important to consider the value of its existence and the value of ours. 

Humanity is not the answer to technology, and technology is not the answer to humanity. But, both of these things do ask the same questions of each other: where do you want your ghost to end up? 

