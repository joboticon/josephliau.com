---
layout: post
title:  "On One's Own"
date:   2019-03-30 09:00:00 -0700
categories: community technology
author: Joseph Liau
summary: What can one truly own?
featureimage: /images/2019/03/own.jpg
---

When you think about all that you own, you will start to realize how false that concept can be. Instead of control, exclusivity, and rights, you are really presented with obligation, liability, and confinement. The name that you have been given, the body in which you travel, and even the children that you brought into this world, are not solely given to you.
 
Nothing is permanent. At best, things are on a long-term loan to you, and yet you still have the duty to maintain them. So, to endeavor to own as many things as possible, is not only futile an unrealistic, but a path to many burdens.

Suddenly the rat race becomes a rats nest; it’s not something you can win, but it’s something that you have to deal with. Ownership means responsibility. When something goes wrong, people will start pointing fingers at the owner. Is it worth the trouble to own everything?

It is easy to say that owning things is a problem when you already have enough. Even so, a self-sufficient individual can only see so far. But, if we can grow and nurture proper community and family then we can share responsibilities, and there will be no excess nor lack. It is the covetousness and jealousy that gets in the way. 

To give up ownership is not to say that we can release people from responsibility, but rather we realize that problems that we deal with, also affect those around us. And, when we see how much that responsibility overlaps, then we can start to work together towards a better outcome. Instead of assignment of labour, we have the sharing of duty. This depends on the point of view and the choice of language that we use. If we lessen the possessiveness in speech and in mind, then the sense of entitlement fades. In place of that we can see action and gratitude.

On the contrary, you will find that people who are adverse to responsibility will often attribute ownership of items and problems by use of possessive language, even in an irregular context. These people would rather push duty on to others; however, they don’t realize that it doesn’t make the problem go away. They will still be disappointed when the expectation is not met.

If you are willing to accept responsibility, then you can gain strength, but also realize that the power is not within the individual, but it is with the whole.



