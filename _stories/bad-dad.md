---
layout: story
title: 坏爸爸 (Bad Dad)
author: Joseph Liau
feature: /images/bad-dad/hbb01.jpg
images:
  - image_path: /images/bad-dad/hbb01.jpg
    title: Part 1
    caption: 坏爸爸总是吵我。
  - image_path: /images/bad-dad/hbb02.jpg
    title: Part 2
    caption: 不让我吃好吃的。
  - image_path: /images/bad-dad/hbb03.jpg
    title: Part 3
    caption: 不让我走在好路。
  - image_path: /images/bad-dad/hbb04.jpg
    title: Part 4
    caption: 不让我跟别的玩儿。坏爸爸。
  - image_path: /images/bad-dad/hbb05.jpg
    title: Part 5
    caption: 但是现在我知道。。。
  - image_path: /images/bad-dad/hbb06.jpg
    title: Part 6
    caption: 为什么。
  - image_path: /images/bad-dad/hbb07.jpg
    title: Part 7
    caption: 爱爸爸。
---
