---
layout: story
title: 蝴蝶 (Butterfly)
author: Joseph Liau
feature: /images/butterfly/hudie01.png
images:
  - image_path: /images/butterfly/hudie01.png
    title: Part 1
    caption: <br />
  - image_path: /images/butterfly/hudie02.png
    title: Part 2
    caption: <br />
  - image_path: /images/butterfly/hudie03.png
    title: Part 3
    caption: <br />
  - image_path: /images/butterfly/hudie04.png
    title: Part 4
    caption: <br />
  - image_path: /images/butterfly/hudie05.png
    title: Part 5
    caption: <br />
  - image_path: /images/butterfly/hudie06.png
    title: Part 6
    caption: <br />
  - image_path: /images/butterfly/hudie07.png
    title: Part 7
    caption: <br />
  - image_path: /images/butterfly/hudie08.png
    title: Part 8
    caption: <br />
  - image_path: /images/butterfly/hudie09.png
    title: Part 9
    caption: <br />
  - image_path: /images/butterfly/hudie10.png
    title: Part 10
    caption: <br />
---

