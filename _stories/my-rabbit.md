---
layout: story
title: 我的兔子 (My Rabbit)
author: Joseph Liau
feature: /images/my-rabbit/wdtz01.png
images:
  - image_path: /images/my-rabbit/wdtz01.png
    title: Part 1
    caption: 哦。
  - image_path: /images/my-rabbit/wdtz02.png
    title: Part 2
    caption: 好可爱。
  - image_path: /images/my-rabbit/wdtz03.png
    title: Part 3
    caption: <br />
  - image_path: /images/my-rabbit/wdtz04.png
    title: Part 4
    caption: 这是我的兔子。
  - image_path: /images/my-rabbit/wdtz05.png
    title: Part 5
    caption: 走了。
  - image_path: /images/my-rabbit/wdtz06.png
    title: Part 6
    caption: 哦回来了。
  - image_path: /images/my-rabbit/wdtz07.png
    title: Part 7
    caption: 这是我的兔子。
  - image_path: /images/my-rabbit/wdtz08.png
    title: Part 8
    caption: 又走了。
  - image_path: /images/my-rabbit/wdtz09.png
    title: Part 9
    caption: ！
  - image_path: /images/my-rabbit/wdtz10.png
    title: Part 10
    caption: 我的兔子呢？
  - image_path: /images/my-rabbit/wdtz11.png
    title: Part 11
    caption: 哦。
  - image_path: /images/my-rabbit/wdtz12.png
    title: Part 12
    caption: <br />
---

