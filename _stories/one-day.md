---
layout: story
title: 有一天 (One Day)
author: Joseph Liau
feature: /images/one-day/yit001.png
images:
  - image_path: /images/one-day/yit001.png
    title: Part 1
    caption: 有一天找到了你。
  - image_path: /images/one-day/yit002.png
    title: Part 2
    caption: <br />
  - image_path: /images/one-day/yit003.png
    title: Part 3
    caption: 你想让我来玩儿。
  - image_path: /images/one-day/yit004.png
    title: Part 4
    caption: 但是我不能去。生病了。
  - image_path: /images/one-day/yit005.png
    title: Part 5
    caption: <br />
  - image_path: /images/one-day/yit006.png
    title: Part 6
    caption: 彼此给礼物
  - image_path: /images/one-day/yit007.png
    title: Part 7
    caption: 有一天你没过来
  - image_path: /images/one-day/yit008.png
    title: Part 8
    caption: 害怕。生病了。
  - image_path: /images/one-day/yit009.png
    title: Part 9
    caption: 算了。
  - image_path: /images/one-day/yit010.png
    title: Part 10
    caption: 找你。
  - image_path: /images/one-day/yit011.png
    title: Part 11
    caption: 可是找不到。然后。。。
  - image_path: /images/one-day/yit012.png
    title: Part 12
    caption: 有一天
---
      
