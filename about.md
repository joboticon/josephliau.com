---
layout: page
title: About
permalink: /about/
---

*No man is an island. But, inevitably he must pass through one.*

Website and content created by Joseph Liau. 

Contact me:
<form
  action="https://formspree.io/f/mzbnjaoz"
  method="POST"
>
  <label>
    Your email:
    <br /><input type="text" name="_replyto">
  </label>
  <label>
    <br />Your message:
    <br /><textarea name="message" cols="50" rows="10"></textarea>
  </label>

  <br /><button type="submit">Send</button>
</form>

