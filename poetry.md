---
layout: default
title: Poetry
permalink: /poetry/
---

<div class="home">

  <h1 class="page-heading">Stories</h1>

   <p>Star Wars Poetry. And some not.</p>

  <ul class="post-list">
    {% for poetry in site.poetry reversed %}
      <li>
        <div class="front-post">
        <div class="front-title">
          <a class="post-link" href="{{ poetry.url | prepend: site.baseurl }}">{{ poetry.title }}</a>
	  {{ poetry.summary }}
        </div>
       </div>
      </li>
    {% endfor %}
  </ul>

</div>